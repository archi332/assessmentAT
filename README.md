## Project initialization locally

##### Docker up
1. `docker-compose build` 
2. `docker-compose up -d`
3. `docker-compose exec php bash`
4. `cron`

##### Console commands
1. **Send sms with temperature info** `php /var/www/command.php SendTemperatureNotification --city=Thessaloniki --temperature=20`

##### Short description
Inside container is installed cron. On build docker there will be created cron job to run every 10 minutes.
To run it you need to build docker and run it, then enter into php container and run command `cron`
Some configs are stored inside file `config.php` at the root directory.
Of course it is not ideal, and there are a lot of things to improve.