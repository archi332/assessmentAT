<?php

use App\Command\CommandInterface;

define('PROJECT_ROOT', __DIR__);
$loader = require PROJECT_ROOT . '/vendor/autoload.php';
require PROJECT_ROOT . '/config.php';

/**
 * @var int|string $key
 * @var int|string|float|bool $value
 */
foreach ($config as $key => $value) {
    if (!in_array(gettype($value), ['string', 'boolean', 'integer', 'float'])) {
        continue;
    }

    putenv(sprintf('%s=%s', $key, $value));
}

// @TODO: probably divide and move to another file (command handler)

// @TODO: validate string
$className = sprintf("App\Command\%sCommand", $argv[1]);

// @TODO: Handle exceptions
$reflection = new ReflectionClass($className);
if ($reflection->implementsInterface(CommandInterface::class)) {

    /** @var CommandInterface $command */
    $command = new $className;

    $command->execute();
}
