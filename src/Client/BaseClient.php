<?php


namespace App\Client;


use App\Enum\RequestEnum;
use App\Response\Response;

/**
 * Class BaseClient
 * @package App\Client
 */
abstract class BaseClient
{
    /** @var string */
    protected $baseUrl;

    /**
     * @param string $uri
     * @param string|null $method
     * @param array|null $parameters ['body' => array, 'headers' => array, 'query' => array, 'base_url' => string]
     *
     * @return Response
     */
    public function call(string $uri, ?string $method = RequestEnum::GET, ?array $parameters = []): Response
    {
        $curl = curl_init();

        $url = $this->setQueryFromParameters(($parameters['base_url'] ?? $this->baseUrl) . $uri, $parameters);

        // Add http methods and their behavior
        switch ($method) {
            case RequestEnum::POST:
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, strtoupper($method));
                curl_setopt($curl, CURLOPT_POSTFIELDS, $this->prepareBodyFromParameters($parameters));
                break;
        }

        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->getHeadersFromParameters($parameters));
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($curl);

        $curlInfo = curl_getinfo($curl);

        $response = new Response($curlInfo['url'], $curlInfo['http_code'], (array)json_decode($result));

        curl_close($curl);

        return $response;
    }

    /**
     * @param array $parameters
     *
     * @return array
     */
    private function getHeadersFromParameters(array $parameters): array
    {
        $headers = [];

        if (isset($parameters['headers']) && is_array($parameters['headers'])) {
            foreach ($parameters['headers'] as $key => $header) {
                $headers[] = sprintf('%s: %s', $key, $header);
            }
        }

        return $headers;
    }

    /**
     * @param array $parameters
     *
     * @return string
     */
    private function prepareBodyFromParameters(array $parameters): string
    {
        $body = [];

        if (isset($parameters['body']) && is_array($parameters['body'])) {
            $body = $parameters['body'];
        }

        if ($body && is_array($body) && $parameters['headers']['Content-Type'] === 'application/x-www-form-urlencoded') {
            $encodedBody = [];
            foreach ($body as $key => $value) {
                $encodedBody[] = sprintf('%s=%s', $key, $value);
            }

            $body = implode('&', $encodedBody);

        } else {

            $body = json_encode($body);
        }

        return $body;
    }

    /**
     * @param string $url
     * @param array $parameters
     *
     * @return string
     */
    private function setQueryFromParameters(string $url, array $parameters): string
    {
        if (isset($parameters['query']) && is_array($parameters['query'])) {
            $url = sprintf("%s?%s", $url, http_build_query($parameters['query']));
        }

        return $url;
    }
}