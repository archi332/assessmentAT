<?php


namespace App\Client;

use App\Response\Response;

/**
 * Class OpenWeatherClient
 * @package App\Client
 */
class OpenWeatherClient extends BaseClient
{
    /** @var string */
    protected $baseUrl = 'https://api.openweathermap.org/data/2.5/';

    /** @var string */
    private $apiKey;

    /** @var string */
    private $metric;

    /**
     * OpenWeatherClient constructor.
     */
    public function __construct()
    {
        $this->apiKey = getenv('openWeatherApiKey');
        $this->metric = getenv('openWeatherUnits');
    }

    /**
     * // @TODO: response should be serialized to object for example `WeatherResponse`
     *
     * @param string $location
     *
     * @return Response
     */
    public function getWeatherByLocation(string $location): Response
    {
        $response = $this->call('weather', null, [
            'query' => [
                'q' => $location,
                'units' => $this->metric,
                'appid' => $this->apiKey
            ]
        ]);

        @trigger_error('Error. OpenWeatherClient. Get weather by location. Error code ' . $response->getStatusCode());

        return $response;
    }
}