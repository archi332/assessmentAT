<?php


namespace App\Client;


use App\Enum\RequestEnum;
use App\Response\Response;

class RouteeClient extends BaseClient
{
    const ACCESS_TOKEN_FILE = '/routee_access_token.json';

    /** @var string  */
    protected $baseUrl = 'https://connect.routee.net/';

    /** @var string  */
    protected $authUrl = 'https://auth.routee.net/';

    /** @var string */
    private $accessToken;

    /**
     * RouteeClient constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->accessToken = $this->auth();
    }

    /**
     * @param string $number
     * @param string $text
     *
     * @return Response
     */
    public function sendSms(string $number, string $text)
    {
        $response = $this->call('sms', RequestEnum::POST, [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->accessToken,
                'Content-Type' => 'application/json',
            ],
            'body' => [
                "body" => $text,
                "to" => $number,
                "from" => getenv('smsFrom'),
            ],
        ]);

        if ($response->getStatusCode() !== RequestEnum::OK_STATUS) {
            @trigger_error('Error. RouteeClient. Send sms. Body: ' . json_encode($response->getBody()));
        }

        return $response;
    }

    /**
     * @throws \Exception
     */
    private function auth(): string
    {
        if ($accessToken = $this->getTokenFromFile()) {
            return $accessToken;
        }

        $authToken = base64_encode(sprintf('%s:%s', getenv('routeeApiKey'), getenv('routeeAppSecret')));

        $parameters = [
            'body' => [
                'grant_type' => 'client_credentials',
            ],
            'headers' => [
                'authorization' => "Basic " . $authToken,
                "Content-Type" => "application/x-www-form-urlencoded",
            ],
            'base_url' => $this->authUrl
        ];

        $response = $this->call('oauth/token', RequestEnum::POST, $parameters);

        if ($response->getStatusCode() === RequestEnum::OK_STATUS) {
            $this->storeTokenToFile($response->getBody());

        } else {

            throw new \Exception('Error. RouteeClient. Authorization failed');
        }

        return $response->getBody()['access_token'];
    }

    /**
     * @return string|null
     *
     * @throws \Exception
     */
    private function getTokenFromFile(): ?string
    {
        if (file_exists(PROJECT_ROOT . self::ACCESS_TOKEN_FILE)) {
            $existedAccessToken = file_get_contents(PROJECT_ROOT . self::ACCESS_TOKEN_FILE);

            $existedAccessToken = json_decode($existedAccessToken);

            if ((new \DateTime())->getTimestamp() < $existedAccessToken->expires_at) {
                return $existedAccessToken->access_token;
            }
        }
        return null;
    }

    /**
     * @param array $body
     *
     * @return bool
     *
     * @throws \Exception
     */
    private function storeTokenToFile(array $body): bool
    {
        $expiresAt = (new \DateTime())
            ->modify(sprintf('+%s seconds', $body['expires_in']))->getTimestamp();

        return file_put_contents(PROJECT_ROOT . self::ACCESS_TOKEN_FILE, json_encode([
            'access_token' => $body['access_token'],
            'expires_at' => $expiresAt,
        ]));
    }
}