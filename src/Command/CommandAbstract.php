<?php


namespace App\Command;


abstract class CommandAbstract
{
    /** @var array  */
    private $arguments = [];

    /**
     * @return array
     */
    protected function getArguments(): array
    {
        if ($this->arguments) {
            return $this->arguments;
        }

        foreach ($_SERVER['argv'] as $argument) {
            if (preg_match('/^--[a-zA-Z0-9]+=[a-zA-Z0-9]+/', $argument) > 0) {
                $data = explode('=', $argument);

                $this->arguments[str_replace('--', '', $data[0])] = $data[1];
            }
        }

        return $this->arguments;
    }

    /**
     * @param string $name
     *
     * @return string|null
     */
    protected function getArgument(string $name): ?string
    {
        return $this->getArguments()[$name] ?? null;
    }
}