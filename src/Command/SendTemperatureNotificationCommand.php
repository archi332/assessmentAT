<?php

namespace App\Command;

use App\Service\TemperatureNotificationService;

/**
 * Class SendTemperatureNotificationCommand
 * @package App\Command
 */
class SendTemperatureNotificationCommand extends CommandAbstract implements CommandInterface
{
    /** @var TemperatureNotificationService */
    private $temperatureNotificationService;

    /**
     * SendTemperatureNotificationCommand constructor.
     */
    public function __construct()
    {
        $this->temperatureNotificationService = new TemperatureNotificationService();
    }

    /**
     * @throws \Exception
     */
    public function execute()
    {
        $city = $this->getArgument('city') ?? 'Thessaloniki';
        $temperature = $this->getArgument('temperature') ?? 20;

        $response = $this->temperatureNotificationService->sendNotificationMoreLessTemperature((float)$temperature, $city);

        var_dump($response);
        die();
    }

}