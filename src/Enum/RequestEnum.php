<?php


namespace App\Enum;


class RequestEnum
{
    const GET = 'get';
    const POST = 'post';

    const OK_STATUS = 200;
}