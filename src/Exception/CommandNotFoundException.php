<?php

namespace App\Exception;

use App\Enum\ExceptionEnum;
use Throwable;

class CommandNotFoundException extends \Exception
{
    const COMMAND_NOT_FOUND_TEMPLATE = 'Command %s not found';

    /**
     * CommandNotFoundException constructor.
     *
     * @param $name
     * @param string $template
     * @param Throwable|null $previous
     */
    public function __construct(string $name, string $template = self::COMMAND_NOT_FOUND_TEMPLATE, Throwable $previous = null)
    {
        parent::__construct($this->prepareMessage($template, $name), ExceptionEnum::NOT_FOUND_CODE, $previous);
    }

    /**
     * @param string $template
     * @param string $name
     *
     * @return string
     */
    private function prepareMessage(string $template, string $name)
    {
        return sprintf($template, $name);
    }
}