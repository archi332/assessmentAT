<?php


namespace App\Response;


class Response
{
    /** @var string */
    private $url;

    /** @var int */
    private $statusCode;

    /** @var array */
    private $body;

    /**
     * Response constructor.
     *
     * @param string $url
     * @param int $statusCode
     * @param array $body
     */
    public function __construct(string $url, int $statusCode, array $body)
    {
        $this->url = $url;
        $this->statusCode = $statusCode;
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @return array
     */
    public function getBody(): array
    {
        return $this->body;
    }
}