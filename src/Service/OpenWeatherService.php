<?php


namespace App\Service;


use App\Client\OpenWeatherClient;
use App\Enum\RequestEnum;
use App\Response\Response;

class OpenWeatherService
{
    /** @var OpenWeatherClient */
    private $openWeatherClient;

    /**
     * OpenWeatherService constructor.
     */
    public function __construct()
    {
        $this->openWeatherClient = new OpenWeatherClient();
    }

    /**
     * @param string $location
     * @return Response
     */
    public function getWeatherByLocation(string $location): Response
    {
        return $this->openWeatherClient->getWeatherByLocation($location);
    }

    /**
     * @param string $location
     *
     * @return float|null
     */
    public function getTemperatureByLocation(string $location): ?float
    {
        $weather = $this->getWeatherByLocation($location);

        if ($weather->getStatusCode() === RequestEnum::OK_STATUS) {
            return $weather->getBody()['main']->temp;

        } else {

            return null;
        }
    }
}