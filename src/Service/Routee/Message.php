<?php


namespace App\Service\Routee;


class Message
{
    private $phoneNumber;
    private $text;

    public function __construct(string $phoneNumber, string $text)
    {
        $this->phoneNumber = $phoneNumber;
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

}