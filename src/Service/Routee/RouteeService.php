<?php

namespace App\Service\Routee;

use App\Client\RouteeClient;
use App\Response\Response;

/**
 * Class RouteeService
 * @package App\Service\Routee
 */
class RouteeService
{
    /** @var RouteeClient */
    private $routeeClient;

    /**
     * RouteeService constructor.
     */
    public function __construct()
    {
        $this->routeeClient = new RouteeClient();
    }

    /**
     * @param Message $message
     *
     * @return Response
     */
    public function prepareSendSms(Message $message): Response
    {
        return $this->routeeClient->sendSms($message->getPhoneNumber(), $message->getText());
    }

}