<?php


namespace App\Service;


use App\Enum\RequestEnum;
use App\Service\Routee\Message;
use App\Service\Routee\RouteeService;

/**
 * Class TemperatureNotificationService
 * @package App\Service
 */
class TemperatureNotificationService
{
    const LARGER_TEMPERATURE_TEMPLATE = '%s, Temperature more than %sC.';
    const SMALLER_TEMPERATURE_TEMPLATE = '%s, Temperature less than %sC.';

    /** @var RouteeService */
    private $routeeService;

    /** @var OpenWeatherService */
    private $openWeatherService;

    /**
     * TemperatureNotificationService constructor.
     */
    public function __construct()
    {
        $this->routeeService = new RouteeService();
        $this->openWeatherService = new OpenWeatherService();
    }

    /**
     * Temperature condition is made equal to task description:
     * `If the temperature is greater than 20C send SMS message to +30 6948872100 with text "Your name and Temperature more than 20C. <the actual temperature>"
     * else send sms message to +30  6948872100 with text "Your name and Temperature less than 20C. <the actual temperature>"`
     *
     * In a fact we need to know, what we should do if temperature is equal to 20.
     *
     * UserName should be extracted from somewhere (for example db, or external service)
     *
     * @param float $temperature
     * @param string $location
     *
     * @return bool Returns true on success, false otherwise
     *
     * @throws \Exception
     */
    public function sendNotificationMoreLessTemperature(float $temperature, string $location): bool
    {
        $temperatureByLocation = $this->openWeatherService->getTemperatureByLocation($location);

        if (!$temperatureByLocation) {
            throw new \Exception('Get weather information error');
        }

        $message = new Message(
            getenv('temperatureNotificationReceiver'),
            $this->getMessageTextByTemperature($temperatureByLocation, $temperature)
        );

        $sendSmsResponse = $this->routeeService->prepareSendSms($message);

        return $sendSmsResponse->getStatusCode() === RequestEnum::OK_STATUS;
    }

    /**
     * @param float $temperatureByLocation
     * @param float $temperature
     *
     * @return string
     */
    private function getMessageTextByTemperature(float $temperatureByLocation, float $temperature): string
    {
        if ($temperatureByLocation > $temperature) {
            $textTemplate = self::LARGER_TEMPERATURE_TEMPLATE;

        } else {

            $textTemplate = self::SMALLER_TEMPERATURE_TEMPLATE;
        }

        return sprintf($textTemplate, 'Dear UserName', $temperatureByLocation);
    }

}